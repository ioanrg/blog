diff --git a/app/controllers/comments_controller.rb b/app/controllers/comments_controller.rb
index ef4324b..9ff46c9 100644
--- a/app/controllers/comments_controller.rb
+++ b/app/controllers/comments_controller.rb
@@ -24,12 +24,11 @@ class CommentsController < ApplicationController
   # POST /comments
   # POST /comments.json
   def create
-    @post = Post.find(params[:post_id])
-    @comment = @post.comments.create(comment_params)
+    @comment = Comment.new(comment_params)
 
     respond_to do |format|
       if @comment.save
-        format.html { redirect_to @post, notice: 'Comment was successfully created.' }
+        format.html { redirect_to @comment, notice: 'Comment was successfully created.' }
         format.json { render :show, status: :created, location: @comment }
       else
         format.html { render :new }
diff --git a/app/controllers/posts_controller.rb b/app/controllers/posts_controller.rb
index 095bd38..16bba99 100644
--- a/app/controllers/posts_controller.rb
+++ b/app/controllers/posts_controller.rb
@@ -1,6 +1,6 @@
 class PostsController < ApplicationController
   before_action :set_post, only: [:show, :edit, :update, :destroy]
-  before_action :authenticate, except: [:index, :show]
+
   # GET /posts
   # GET /posts.json
   def index
@@ -62,20 +62,13 @@ class PostsController < ApplicationController
   end
 
   private
+    # Use callbacks to share common setup or constraints between actions.
+    def set_post
+      @post = Post.find(params[:id])
+    end
 
-  # Use callbacks to share common setup or constraints between actions.
-  def set_post
-    @post = Post.find(params[:id])
-  end
-
-  # Never trust parameters from the scary internet, only allow the white list through.
-  def post_params
-    params.require(:post).permit(:title, :body)
-  end
-
-  def authenticate
-    authenticate_or_request_with_http_basic do |name, password|
-      name == "admin" && password == "secret"
+    # Never trust parameters from the scary internet, only allow the white list through.
+    def post_params
+      params.require(:post).permit(:title, :body)
     end
-  end
 end
diff --git a/app/models/comment.rb b/app/models/comment.rb
index 259ed1f..45b2d38 100644
--- a/app/models/comment.rb
+++ b/app/models/comment.rb
@@ -1,4 +1,2 @@
 class Comment < ActiveRecord::Base
-  validates_presence_of :post_id, :body
-  belongs_to :post
 end
diff --git a/app/models/post.rb b/app/models/post.rb
index bd970f9..791dcb5 100644
--- a/app/models/post.rb
+++ b/app/models/post.rb
@@ -1,4 +1,2 @@
 class Post < ActiveRecord::Base
-  validates_presence_of :title, :body
-  has_many :comments, dependent: :destroy
 end
diff --git a/app/views/posts/show.html.erb b/app/views/posts/show.html.erb
index 3e9e3cd..c14ef10 100644
--- a/app/views/posts/show.html.erb
+++ b/app/views/posts/show.html.erb
@@ -1,35 +1,14 @@
-<p id="notice">
-	<%= notice %>
-</p>
+<p id="notice"><%= notice %></p>
 
 <p>
-	<strong>Title:</strong>
-	<%= @post.title %>
+  <strong>Title:</strong>
+  <%= @post.title %>
 </p>
 
 <p>
-	<strong>Body:</strong>
-	<%= @post.body %>
-</p>
-<h2>Comments</h2>
-<div id="comments">
-	<% @post.comments.each do |comment| %>
-	<%= div_for comment do %>
-	<p>
-		<strong>Posted <%= time_ago_in_words(comment.created_at) %></strong>
-		<br />
-		<%= h(comment.body) %>
-	</p>
-	<% end %>
-	<% end %>
-</div>
-<%= form_for([@post, Comment.new]) do |f| %>
-<p>
-	<%= f.label :body, "New comment"%>
-	<br />
-	<%= f.text_area :body %>
+  <strong>Body:</strong>
+  <%= @post.body %>
 </p>
-<p></p><%= f.submit "Add Comment" %></p>
-<% end%>
+
 <%= link_to 'Edit', edit_post_path(@post) %> |
 <%= link_to 'Back', posts_path %>
diff --git a/config/routes.rb b/config/routes.rb
index 74985b9..6dd10ef 100644
--- a/config/routes.rb
+++ b/config/routes.rb
@@ -1,9 +1,8 @@
 Rails.application.routes.draw do
- # resources :comments
-  
-  resources :posts do
-    resources :comments
-  end
+  resources :comments
+
+  resources :posts
+
   # The priority is based upon order of creation: first created -> highest priority.
   # See how all your routes lay out with "rake routes".
 
